#include <iostream>
using namespace std;

void swap(int &a, int &b) {
    int temp = a;
    a = b;
    b = temp;

}

int main() {

    int numbers[5] = {16, 2, 77, 40, 12071};
    int myArraySize = sizeof(numbers) / sizeof(numbers[0]);

    for (int i = 0; i < myArraySize; i++) {
        for (int j = i + 1; j < myArraySize; j++) {
            if (numbers[i] > numbers[j])swap(numbers[i], numbers[j]);
        }
    }

    //Print all numbers:
    cout<<"Sorted:"<<endl;
    for (int i = 0; i < myArraySize; i++) {
        cout << numbers[i] << endl;
    }


    return 0;
}
